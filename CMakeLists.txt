# Cross-platform Makefile generation.
# This is used purely for the unit tests
cmake_minimum_required(VERSION 3.14)
#set(CMAKE_POLICY_DEFAULT_CMP0048 NEW)
set(CMAKE_POLICY_DEFAULT_CMP0077 NEW)

#include local CMake environment.
#include(CMakeDependentOption)

set(_project front-end)
project(front-end 
  VERSION 1.0
)
###################
# Options
# Note need to place after project to ensure the CROSSCOMPILING is used.

# Makes BUILD_TEST available as option for all builds except when cross compiling.
option(BUILD_TEST "Build all tests." ON)

#Makes STATIC_ANALYSIS available as option for all builds except when cross compiling.
option(STATIC_ANALYSIS "Performs static analysis on tests." OFF)

# Makes BUILD_DOC available as option for all builds.
option(BUILD_DOC  "Build all documentation." OFF)

option(VERILOG_TEST "Support Verilog testing." ON)

###################
# Common Includes and paths.

# Create a common cmake path for identification of modules etc.
list( APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake/Modules" )
message(STATUS "Module Path: ${CMAKE_MODULE_PATH}")

# Add in CCache if available.
find_package(CCache)

# Ensure Build dir isn't same as local dir.
if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
    message( FATAL_ERROR "Do not perform build in source directory. Make a separate directory for build:\n\tmkdir build; cd build; cmake ..\n And ensure it is empty.")
endif()


#Limit it to Makefile or Ninja for now.
if ( NOT CMAKE_BUILD_TYPE AND ( (CMAKE_GENERATOR MATCHES ".*Makefile.*") OR (CMAKE_GENERATOR MATCHES ".*Ninja.*")))
    message( STATUS "Makefile generator detected and build type not defined. Defaulting to `release`.")
    set( CMAKE_BUILD_TYPE release CACHE STRING "Choose the type of build" FORCE )
endif()

# Common Functions and Macros
file( GLOB _functions ${CMAKE_CURRENT_LIST_DIR}/cmake/functions/*.cmake )
foreach( _f ${_functions} )
  include( ${_f} )
endforeach()

###################
# Default Package includes

# Static Analysis - if STATIC_ANALYSIS is ON.
find_package(DefaultStaticAnalysis REQUIRED)

#Testing:
find_package(DefaultTest REQUIRED)

#Documentation:
set(DOC_EXCLUDE_PATTERNS  
  */build*/*) # Add your exclude patterns to documentation.
find_package(Doc REQUIRED)

#Use Current Verilog Test
find_package(VerilogTest REQUIRED 4.0)
