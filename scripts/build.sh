
# Ensure have appropriate environment
sudo apt-get install cmake

cd ../..
export CAD_ENV_ROOT=$CWD

cd ..
mkdir build

# Build the doxygen-verilog code.
mkdir build/doxygen-verilog
cd build/doxygen-verilog
cmake -G "Unix Makefiles" ../../submodules/third-party/doxygen-verilog
make
sudo make install
cd ../..

# Get the latest version of plantuml.
mkdir build/plantuml
cd build/plantuml
wget http://sourceforge.net/projects/plantuml/files/plantuml.1.2017.18.jar/download
mv download plantuml.jar
cd ../..

# Get the latest version of wavedrom-editor
#mkdir build/wavedrom
#cd build/wavedrom
#wget https://github.com/wavedrom/wavedrom.github.io/releases/download/v1.6.2/wavedrom-editor-v1.6.2-linux-x64.tar.gz
#tar -xvf wavedrom-editor-v1.6.2-linux-x64.tar.gz
#sudo apt-get install libgconf-2.4

	
# Build the latest version of iverilog (Icarus) - free Verilog compiler.
sudo apt-get install bison flex gperf termcap autoconf cppcheck
cd submodules/third-party/iverilog
autoconf
./configure
make
make check
sudo make install
#sudo make uninstall

# Build the latest version of verilator - free Verilog compiler.
sudo apt-get install git make autoconf g++ flex bison   # First time prerequisites
# Each build:
unset VERILATOR_ROOT  # For bash
cd submodules/third-party/verilator
autoconf        # Create ./configure script
./configure
make
sudo make install
